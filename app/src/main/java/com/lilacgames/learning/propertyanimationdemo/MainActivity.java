package com.lilacgames.learning.propertyanimationdemo;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.renderscript.Sampler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.activity_main);
        TextView text_view = (TextView) findViewById(R.id.textview_1);
        ImageView image_view = (ImageView) findViewById(R.id.nought_image);

        ValueAnimator topxdir = ObjectAnimator.ofFloat(image_view,"x",0,800);
        topxdir.setDuration(3000);
        topxdir.setRepeatMode(ValueAnimator.RESTART);
        // TODO: 6/10/16 Add getHeight() feature
        ValueAnimator rightdir = ObjectAnimator.ofFloat(image_view,"y",image_view.getY()+50,image_view.getY()+1500);
        rightdir.setDuration(3000);
        rightdir.setRepeatMode(ValueAnimator.RESTART);
        ValueAnimator bottomdir = ObjectAnimator.ofFloat(image_view,"x",image_view.getX(),-800);
        bottomdir.setDuration(3000);

        ValueAnimator move = ObjectAnimator.ofFloat(text_view,"x",0,400);
        move.setDuration(3000);


        AnimatorSet rotate_animator = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this,R.animator.rotate_text);
        rotate_animator.setTarget(text_view);
        rotate_animator.play(move);
        rotate_animator.start();

        AnimatorSet transit_circle = new AnimatorSet();
        transit_circle.play(topxdir).before(rightdir);
//        transit_circle.play(bottomdir);

        transit_circle.start();

//        AnimatorSet transit_image = (AnimatorSet) AnimatorInflater.loadAnimator(MainActivity.this,R.animator.transit_image);
//        transit_image.playSequentially();
//        transit_image.
//        transit_image.setTarget(image_view);
//        transit_image.start();


    }
}
